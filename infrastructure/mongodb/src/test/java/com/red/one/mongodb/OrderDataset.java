package com.red.one.mongodb;

import com.red.one.domain.entities.Order;
import com.red.one.domain.entities.OrderItem;
import com.red.one.domain.entities.OrderStatus;
import com.red.one.mongodb.entities.OrderEntity;
import com.red.one.mongodb.entities.OrderItemEntity;

import java.util.List;
import java.util.UUID;

public final class OrderDataset {

    public static Order getOrder(final UUID id, final List<OrderItem> orderItems, final OrderStatus status, final Long total) {
        Order domain = new Order();
        domain.setId(id);
        domain.setOrderItems(orderItems);
        domain.setStatus(status);
        domain.setTotal(total);
        return domain;
    }

    public static OrderEntity getOrderEntity(final UUID id, final List<OrderItemEntity> orderItems, final OrderStatus status, final Long total) {
        OrderEntity entity = new OrderEntity();
        entity.setId(id);
        entity.setOrderItems(orderItems);
        entity.setStatus(status);
        entity.setTotal(total);
        return entity;
    }
}
