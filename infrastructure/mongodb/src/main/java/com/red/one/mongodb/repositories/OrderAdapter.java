package com.red.one.mongodb.repositories;

import com.red.one.domain.entities.Order;
import com.red.one.domain.ports.OrderPort;
import com.red.one.mongodb.mappers.OrderMapper;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

import java.util.UUID;

@Component
public class OrderAdapter implements OrderPort {

    private final OrderRepository repository;
    private final OrderMapper mapper;

    public OrderAdapter(final OrderRepository repository, final OrderMapper mapper) {
        this.repository = repository;
        this.mapper = mapper;
    }

    @Override
    public Mono<Order> findById(final UUID id) {
        return repository.findById(id).map(mapper::map);
    }

    @Override
    public Mono<Order> save(final Order order) {
        return this.repository.save(this.mapper.map(order)).map(mapper::map);
    }
}