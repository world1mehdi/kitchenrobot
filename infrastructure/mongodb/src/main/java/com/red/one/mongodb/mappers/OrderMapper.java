package com.red.one.mongodb.mappers;

import com.red.one.domain.entities.Order;
import com.red.one.domain.entities.OrderItem;
import com.red.one.mongodb.entities.OrderEntity;
import com.red.one.mongodb.entities.OrderItemEntity;
import org.mapstruct.Mapper;
import org.mapstruct.NullValueCheckStrategy;

@Mapper(componentModel = "spring", nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface OrderMapper {

    OrderEntity map(final Order domain);

    Order map(final OrderEntity entity);
}
