package com.red.one.mongodb.mappers;

import com.red.one.domain.entities.Dish;
import com.red.one.mongodb.entities.DishEntity;
import org.mapstruct.Mapper;
import org.mapstruct.NullValueCheckStrategy;

@Mapper(componentModel = "spring", nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface DishMapper {

    DishEntity map(final Dish domain);

    Dish map(final DishEntity entity);
}
