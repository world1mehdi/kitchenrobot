package com.red.one.mongodb.entities;

import java.io.Serializable;
import java.util.UUID;

public class OrderItemEntity implements Serializable {

    private UUID orderId;
    private UUID dishId;
    private Long quantity;

    public OrderItemEntity() {
    }

    public UUID getOrderId() {
        return orderId;
    }

    public void setOrderId(UUID orderId) {
        this.orderId = orderId;
    }

    public UUID getDishId() {
        return dishId;
    }

    public void setDishId(UUID dishId) {
        this.dishId = dishId;
    }

    public Long getQuantity() {
        return quantity;
    }

    public void setQuantity(Long quantity) {
        this.quantity = quantity;
    }
}
