package com.red.one.mongodb.repositories;

import com.red.one.domain.entities.Dish;
import com.red.one.domain.ports.DishPort;
import com.red.one.mongodb.mappers.DishMapper;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.UUID;

@Component
public class DishAdapter implements DishPort {

    private final DishRepository repository;
    private final DishMapper mapper;

    public DishAdapter(final DishRepository repository, final DishMapper mapper) {
        this.repository = repository;
        this.mapper = mapper;
    }

    @Override
    public Mono<Dish> findById(final UUID id) {
        return repository.findById(id).map(mapper::map);
    }

    @Override
    public Flux<Dish> findAll() {
        return repository.findAll().map(mapper::map);
    }

    @Override
    public Mono<Dish> save(final Dish domain) {
        return repository.save(this.mapper.map(domain))
                .map(mapper::map);
    }
}