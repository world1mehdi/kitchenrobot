package com.red.one.mongodb.entities;

import com.red.one.domain.entities.OrderStatus;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;
import java.util.List;
import java.util.UUID;

@Document(collection = "orderDishes")
public class OrderEntity extends AuditEntity implements Serializable {

    private UUID id;
    private OrderStatus status;
    private List<OrderItemEntity> orderItems;
    private Long total;

    public OrderEntity(UUID id, OrderStatus status, List<OrderItemEntity> orderItems, Long total) {
        this.id = id;
        this.status = status;
        this.orderItems = orderItems;
        this.total = total;
    }

    public OrderEntity() {
    }

    public UUID getId() {
        return id;
    }

    public OrderStatus getStatus() {
        return status;
    }

    public List<OrderItemEntity> getOrderItems() {
        return orderItems;
    }

    public Long getTotal() {
        return total;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public void setStatus(OrderStatus status) {
        this.status = status;
    }

    public void setOrderItems(List<OrderItemEntity> orderItems) {
        this.orderItems = orderItems;
    }

    public void setTotal(Long total) {
        this.total = total;
    }
}
