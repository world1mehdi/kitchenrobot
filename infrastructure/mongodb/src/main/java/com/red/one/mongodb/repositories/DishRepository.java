package com.red.one.mongodb.repositories;

import com.red.one.mongodb.entities.DishEntity;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface DishRepository extends ReactiveMongoRepository<DishEntity, UUID> {
}
