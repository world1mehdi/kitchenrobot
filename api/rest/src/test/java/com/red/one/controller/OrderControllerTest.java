package com.red.one.controller;

import com.red.one.controller.dtos.DishDto;
import com.red.one.controller.dtos.OrderDto;
import com.red.one.controller.mappers.OrderDtoMapper;
import com.red.one.domain.entities.Dish;
import com.red.one.domain.entities.Order;
import com.red.one.domain.services.OrderService;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import javax.annotation.PostConstruct;

public class OrderControllerTest extends BaseTest{

    @Mock
    private OrderService orderService;

    @Mock
    private OrderDtoMapper orderDtoMapper;

    @InjectMocks
    private OrderController orderController;

    @Test
    public void testCreateOrder_whenExistsData_shouldCallService() throws Exception {
        final DishDto expectedDto = OrderDtoDataset.getDishDto();
        final OrderDto expectedOrderDto = OrderDtoDataset.getOrderDto(expectedDto.getId());
        final Order expectedOrder = OrderDtoDataset.getOrder(expectedDto.getId());
        final Dish Expected = OrderDtoDataset.getDish();

        mockMapDishToApi(Expected);
        mockCreateOrder(Mono.just(expectedOrder));
        mockMapOrderToApi(expectedOrderDto);


        orderController.create(expectedDto).block();


        // Mocks verifying
        Mockito.verify(this.orderService, Mockito.times(1))
                .createOrder(Mockito.any(Dish.class));
        Mockito.verifyNoMoreInteractions(this.orderService);
    }

    @Test
    public void testCreateOrder_whenExistsData_shouldReturnExpectedValues() throws Exception {
        final DishDto expectedDto = OrderDtoDataset.getDishDto();
        final OrderDto expectedOrderDto = OrderDtoDataset.getOrderDto(expectedDto.getId());
        final Order expectedOrder = OrderDtoDataset.getOrder(expectedDto.getId());
        final Dish Expected = OrderDtoDataset.getDish();

        mockMapDishToApi(Expected);
        mockCreateOrder(Mono.just(expectedOrder));
        mockMapOrderToApi(expectedOrderDto);


        Mono<OrderDto> result = orderController.create(expectedDto);


        StepVerifier
                .create(result)
                .expectNext(expectedOrderDto)
                .expectComplete()
                .verify();
    }

    private void mockMapDishToApi(Dish expected) {
        Mockito.when(orderDtoMapper.map(Mockito.any(DishDto.class))).thenReturn(expected);
    }

    private void mockCreateOrder(Mono<Order> expected) {
        Mockito.when(orderService.createOrder(Mockito.any(Dish.class))).thenReturn(expected);
    }

    private void mockMapOrderToApi(OrderDto expected) {
        Mockito.when(orderDtoMapper.map(Mockito.any(Order.class))).thenReturn(expected);
    }
}