package com.red.one.controller;

import com.red.one.controller.dtos.DishDto;
import com.red.one.controller.dtos.OrderDto;
import com.red.one.controller.dtos.OrderItemDto;
import com.red.one.domain.entities.Dish;
import com.red.one.domain.entities.Order;
import com.red.one.domain.entities.OrderItem;
import com.red.one.domain.entities.OrderStatus;

import java.util.Arrays;
import java.util.UUID;

public final class OrderDtoDataset {

    public static DishDto getDishDto() {
        DishDto dishDto = new DishDto();
        dishDto.setId(UUID.randomUUID());
        dishDto.setIngredients("cebolla, patata y algo");
        dishDto.setName("paella");
        dishDto.setType("food");
        return dishDto;
    }

    public static OrderDto getOrderDto(final UUID dishId) {
        OrderDto dto = new OrderDto();
        dto.setId(UUID.randomUUID());
        dto.setStatus(OrderStatus.CREATED);
        dto.setTotal(1L);
        OrderItemDto orderItemDto = new OrderItemDto();
        orderItemDto.setDishId(dishId);
        orderItemDto.setOrderId(dto.getId());
        orderItemDto.setQuantity(1L);
        dto.setOrderItems(Arrays.asList(orderItemDto));
        return dto;
    }

    public static Order getOrder(final UUID dishId) {
        Order domain = new Order();
        domain.setId(UUID.randomUUID());
        domain.setStatus(OrderStatus.CREATED);
        domain.setTotal(1L);
        OrderItem orderItem = new OrderItem();
        orderItem.setDishId(dishId);
        orderItem.setOrderId(domain.getId());
        orderItem.setQuantity(1L);
        domain.setOrderItems(Arrays.asList(orderItem));
        return domain;
    }

    public static Dish getDish() {
        Dish dish = new Dish();
        dish.setId(UUID.randomUUID());
        dish.setIngredients("cebolla, patata y algo");
        dish.setName("paella");
        dish.setType("food");
        return dish;
    }
}
