package com.red.one.controller.mappers;

import com.red.one.controller.dtos.DishDto;
import com.red.one.controller.dtos.OrderDto;
import com.red.one.domain.entities.Order;
import com.red.one.domain.entities.Dish;
import org.mapstruct.Mapper;
import org.mapstruct.NullValueCheckStrategy;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Mapper(componentModel = "spring", nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public abstract class OrderDtoMapper {

    public abstract Order map(final OrderDto dto);

    public abstract OrderDto map(final Order dto);

    public abstract DishDto map(final Dish domain);

    public abstract Dish map(final DishDto dto);

    public List<Dish> map(final List<DishDto> dtos){
        return dtos.stream().filter(Objects::nonNull).map(this::map).collect(Collectors.toList());
    }
}