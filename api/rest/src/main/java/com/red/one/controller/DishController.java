package com.red.one.controller;

import com.red.one.controller.dtos.DishDto;
import com.red.one.controller.mappers.DishDtoMapper;
import com.red.one.domain.services.IDishService;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/dishes")
public class DishController {

    private final IDishService service;
    private final DishDtoMapper mapper;

    public DishController(final IDishService service, final DishDtoMapper mapper) {
        this.service = service;
        this.mapper = mapper;
    }

    @PostMapping("/")
    Mono<DishDto> create(@RequestBody final DishDto dto) {
        return service.create(mapper.map(dto)).map(mapper::map);
    }

    @GetMapping(value = "/")
    @ResponseBody
    Flux<DishDto> getAll() {
        return service.getAll().map(mapper::map);
    }
}