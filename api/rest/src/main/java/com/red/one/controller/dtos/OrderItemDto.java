package com.red.one.controller.dtos;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
public class OrderItemDto {
    private UUID orderId;
    private UUID dishId;
    private Long quantity;
}