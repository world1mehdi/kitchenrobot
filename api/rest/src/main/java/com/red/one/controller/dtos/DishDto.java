package com.red.one.controller.dtos;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalTime;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
public class DishDto {
    @JsonProperty("id")
    private UUID id;
    @JsonProperty("type")
    private String type;
    @JsonProperty("name")
    private String name;
    @JsonProperty("ingredients")
    private String ingredients;
    @JsonProperty("modifiedAt")
    private LocalTime modifiedAt;
    @JsonProperty("version")
    private Long version;
}