package com.red.one.controller;

import com.red.one.controller.dtos.DishDto;
import com.red.one.controller.dtos.OrderDto;
import com.red.one.controller.mappers.OrderDtoMapper;
import com.red.one.domain.entities.Order;
import com.red.one.domain.services.IOrderService;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/orders")
public class OrderController {

    private final IOrderService service;

    private final OrderDtoMapper mapper;

    public OrderController(final IOrderService service, final OrderDtoMapper mapper) {
        this.service = service;
        this.mapper = mapper;
    }

    @PostMapping("/")
    Mono<OrderDto> create(@RequestBody final DishDto dto) {
        return service.createOrder(mapper.map(dto)).map(mapper::map);
    }

    @PostMapping(value = "/{id}/dishes", consumes = MediaType.APPLICATION_JSON_VALUE)
    void addAll(@PathVariable final UUID id, @RequestBody final List<DishDto> dtos) {
        service.addAll(id, mapper.map(dtos));
    }

    @PostMapping(value = "/{id}/dish", consumes = MediaType.APPLICATION_JSON_VALUE)
    Mono<Order> addAll(@PathVariable final UUID id, @RequestBody final DishDto dto) {
        return service.add(id, mapper.map(dto));
    }

    @DeleteMapping(value = "/{id}/dishes", consumes = MediaType.APPLICATION_JSON_VALUE)
    void cancel(@PathVariable final UUID id) {
        service.cancel(id);
    }

    @PostMapping("/{id}/complete")
    void completeOrder(@PathVariable final UUID id) {
        service.completeOrder(id);
    }
}
