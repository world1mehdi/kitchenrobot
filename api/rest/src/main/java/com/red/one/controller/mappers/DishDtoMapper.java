package com.red.one.controller.mappers;

import com.red.one.controller.dtos.DishDto;
import com.red.one.domain.entities.Dish;
import org.mapstruct.Mapper;
import org.mapstruct.NullValueCheckStrategy;

@Mapper(componentModel = "spring", nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface DishDtoMapper {

    Dish map(final DishDto dto);

    DishDto map(final Dish domain);
}
