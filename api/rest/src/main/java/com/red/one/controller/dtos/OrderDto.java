package com.red.one.controller.dtos;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.red.one.domain.entities.OrderItem;
import com.red.one.domain.entities.OrderStatus;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
public class OrderDto {
    @JsonProperty("id")
    private UUID id;
    @JsonProperty("status")
    private OrderStatus status;
    @JsonProperty("items")
    private List<OrderItemDto> orderItems;
    @JsonProperty("total")
    private Long total;
}
