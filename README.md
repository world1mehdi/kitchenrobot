# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
Rest API for a Kitchen Robot that will allow a client of the API to get menu and book it
* Version

### How do I get set up? ###

* Summary of set up
* Configuration
The application is configured with docker as container with file called docker-composite inside resources of application module folder.
The database is MongoDB where it is running in container, so before running app; make sure you launch containaer with command:
sudo docker-compose up --build --force-recreate --renew-anon-volumes
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact