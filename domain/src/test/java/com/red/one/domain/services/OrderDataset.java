package com.red.one.domain.services;

import com.red.one.domain.entities.Dish;
import com.red.one.domain.entities.Order;
import com.red.one.domain.entities.OrderItem;
import com.red.one.domain.entities.OrderStatus;

import java.util.List;
import java.util.UUID;

public final class OrderDataset {

    public static Order getOrder(final UUID id, final List<OrderItem> orderItems, final OrderStatus status, final Long total) {
        Order domain = new Order();
        domain.setId(id);
        domain.setOrderItems(orderItems);
        domain.setStatus(status);
        domain.setTotal(total);
        return domain;
    }

    public static Dish getDish() {
        Dish dish = new Dish();
        dish.setId(UUID.randomUUID());
        dish.setType("food");
        dish.setName("Paella");
        dish.setIngredients("cebolla, pescado, tomate ...");
        return dish;
    }
}
