package com.red.one.domain.entities;

import com.red.one.domain.exception.DomainException;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.*;

import java.time.LocalTime;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

@Getter
@Setter
@EqualsAndHashCode(of = {"id", "total"})
@NoArgsConstructor
public class Order {

    public static final long ONE_DISH = 1L;
    private UUID id;
    private OrderStatus status;
    private List<OrderItem> orderItems;
    private Long total;
    private Long version;
    private LocalTime modifiedAt;

    public Order(final UUID id, final List<OrderItem> items) {
        this.id = id;
        this.orderItems = items;
        this.status = OrderStatus.CREATED;
        this.total = items != null ? items.stream().mapToLong(OrderItem::getQuantity).sum() : null;
    }

    public Order(final UUID id, final OrderItem items) {
        this.id = id;
        this.orderItems = Arrays.asList(items);
        this.status = OrderStatus.CREATED;
        this.total = items != null ? items.getQuantity() : null;
    }

    public void complete() {
        validateState();
        this.status = OrderStatus.COMPLETED;
    }

    public void addDish(final UUID orderId, final Dish dish) {
        validateState();
        validateDish(dish);
        orderItems.add(new OrderItem(orderId, dish.getId(), ONE_DISH));
        total = Long.sum(total, ONE_DISH);
    }

    public void addDishes(final UUID orderId, final List<Dish> dish) {
        validateState();
        int size = dish.size();
        final Long quantity = Long.valueOf(size);
        dish.stream().filter(Objects::nonNull)
                .forEach(item ->
                {
                    validateDish(item);
                    orderItems.add(new OrderItem(orderId, item.getId(), quantity));
                    total = Long.sum(total, quantity);
                });
    }

    public void cancelOrder() {
        validateState();
        validateTime();
        status = OrderStatus.CANCELED;
    }

    private void validateTime() {
        LocalTime localTime = modifiedAt.minusSeconds(30);
        if (LocalTime.now().isBefore(localTime)) {
            throw new DomainException("The order cannot be cancel after 30' left.");
        }
    }

    private OrderItem getOrderItem(final UUID id) {
        return orderItems.stream()
                .filter(orderItem -> orderItem.getDishId()
                        .equals(id))
                .findFirst()
                .orElseThrow(() -> new DomainException("Product with " + id + " doesn't exist."));
    }

    private void validateState() {
        if (OrderStatus.COMPLETED.equals(status)) {
            throw new DomainException("The order is in completed state.");
        }
    }

    private void validateDish(final Dish dish) {
        if (dish == null) {
            throw new DomainException("The product cannot be null.");
        }
    }
}
