package com.red.one.domain.services;

import com.red.one.domain.entities.Dish;
import com.red.one.domain.entities.Order;
import com.red.one.domain.entities.OrderItem;
import com.red.one.domain.ports.OrderPort;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

import java.util.List;
import java.util.UUID;

@Service
public class OrderService implements IOrderService {

    private final OrderPort port;

    public OrderService(final OrderPort port) {
        this.port = port;
    }

    @Override
    public Mono<Order> add(final UUID orderId, final Dish dish) {
        return getOrder(orderId)
                .flatMap(result -> {
                    result.addDish(orderId, dish);
                    return port.save(result);
                });
    }

    @Override
    public Mono<Order> addAll(final UUID orderId, final List<Dish> dishes) {
        return getOrder(orderId)
                .flatMap(result -> {
                    result.addDishes(orderId, dishes);
                    return port.save(result);
                });
    }

    public Mono<Order> createOrder(final Dish dish) {
        Long quantity = 1L;
        UUID orderId = UUID.randomUUID();
        final OrderItem orderItem = new OrderItem(orderId, dish.getId(), quantity);
        final Order order = new Order(orderId, orderItem);
        return port.save(order);
    }

    @Override
    public Mono<Order> completeOrder(final UUID id) {
        return getOrder(id)
                .doOnNext(order -> order.complete()).flatMap(order -> port.save(order));
    }

    @Override
    public Mono<Order> cancel(final UUID id) {
        return getOrder(id)
                .doOnNext(order -> order.cancelOrder()).flatMap(order -> port.save(order));
    }

    private Mono<Order> getOrder(UUID id) {
        return port
                .findById(id)
                .switchIfEmpty(Mono.error(new RuntimeException("Order with given id doesn't exist")));
    }
}
