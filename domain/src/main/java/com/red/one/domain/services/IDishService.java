package com.red.one.domain.services;

import com.red.one.domain.entities.Dish;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface IDishService {

    Mono<Dish> create(final Dish dish);

    Flux<Dish> getAll();
}
