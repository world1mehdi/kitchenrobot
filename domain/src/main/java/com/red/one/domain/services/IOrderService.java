package com.red.one.domain.services;

import com.red.one.domain.entities.Dish;
import com.red.one.domain.entities.Order;
import reactor.core.publisher.Mono;

import java.util.List;
import java.util.UUID;

public interface IOrderService {

    Mono<Order> add(final UUID id, final Dish dish);

    Mono<Order> addAll(final UUID id, final List<Dish> dish);

    Mono<Order> createOrder(final Dish dish);


    Mono<Order> completeOrder(final UUID id);

    Mono<Order> cancel(final UUID id);
}
