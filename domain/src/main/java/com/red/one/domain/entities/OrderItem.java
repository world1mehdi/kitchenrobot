package com.red.one.domain.entities;

import lombok.*;

import java.util.UUID;

@Getter
@Setter
@EqualsAndHashCode(of = {"dishId","total"})
@NoArgsConstructor
@AllArgsConstructor
public class OrderItem {
    private UUID orderId;
    private UUID dishId;
    private Long quantity;
}
