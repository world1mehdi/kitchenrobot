package com.red.one.domain.services;

import com.red.one.domain.entities.Dish;
import com.red.one.domain.ports.DishPort;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.UUID;

@Service
public class DishService implements IDishService {

    private final DishPort port;

    public DishService(final DishPort port) {
        this.port = port;
    }

    @Override
    public Mono<Dish> create(final Dish dish) {
        return Mono.just(UUID.randomUUID()).map(uuid -> {
            dish.setId(uuid);
            return dish;
        }).flatMap(domain -> port.save(domain));
    }

    @Override
    public Flux<Dish> getAll() {
        return port.findAll();
    }
}
