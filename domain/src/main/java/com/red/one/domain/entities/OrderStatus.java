package com.red.one.domain.entities;

public enum OrderStatus {
    CREATED, COMPLETED,CANCELED
}
