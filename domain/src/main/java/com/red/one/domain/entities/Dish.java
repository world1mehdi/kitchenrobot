package com.red.one.domain.entities;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalTime;
import java.util.UUID;

@Getter
@Setter
@EqualsAndHashCode(of = "id")
@NoArgsConstructor
public class Dish {
    private UUID id;
    private String name;
    private String type;
    private String ingredients;
    private LocalTime modifiedAt;
    private Long version;
}
