package com.red.one.domain.ports;

import com.red.one.domain.entities.Order;
import reactor.core.publisher.Mono;

import java.util.UUID;

public interface OrderPort {

    Mono<Order> findById(UUID id);

    Mono<Order> save(Order order);
}
