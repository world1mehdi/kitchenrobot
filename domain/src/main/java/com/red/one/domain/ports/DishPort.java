package com.red.one.domain.ports;

import com.red.one.domain.entities.Dish;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.UUID;

public interface DishPort {

    Mono<Dish> findById(UUID id);

    Flux<Dish> findAll();

    Mono<Dish> save(Dish dish);
}
